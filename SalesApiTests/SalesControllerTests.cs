using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using tech_test_payment_api;
using tech_test_payment_api.Models;
using System.Net.Http.Json;

namespace SalesApi.Tests
{
    public class SalesControllerTests : IClassFixture<WebApplicationFactory<tech_test_payment_api.Controllers.SalesController>>
    {
        private readonly WebApplicationFactory<tech_test_payment_api.Controllers.SalesController> _factory;

        public SalesControllerTests(WebApplicationFactory<tech_test_payment_api.Controllers.SalesController> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task RegisterSale_ReturnsOkResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleRequest = new SaleRequest
            {
                Seller = new Seller
                {
                    Id = "1",
                    Cpf = "123456789",
                    Name = "Jorge",
                    Email = "jorge@example.com",
                    Phone = "1234567890"
                },
                Items = new List<Item>
                {
                    new Item
                    {
                        Id = "1",
                        Name = "Produto 1",
                        Price = 10,
                        Quantity = 2
                    }
                }
            };

            // Act
            var response = await client.PostAsJsonAsync("/api/sales/RegistrarVenda", saleRequest);

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetSale_WithValidId_ReturnsOkResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleId = "1"; // ID válido de uma venda existente

            // Act
            var response = await client.GetAsync($"/api/sales/BuscarVenda?id={saleId}");

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetSale_WithInvalidId_ReturnsNotFoundResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleId = "invalid-id";

            // Act
            var response = await client.GetAsync($"/api/sales/BuscarVenda?id={saleId}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task UpdateSaleStatus_WithValidIdAndStatus_ReturnsOkResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleId = "1";
            var saleStatus = SaleStatus.PagamentoAprovado;

            // Act
            var response = await client.PutAsync($"/api/sales/AtualizarVenda?id={saleId}&status={saleStatus}", null);

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task UpdateSaleStatus_WithInvalidId_ReturnsNotFoundResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleId = "invalid-id";
            var saleStatus = SaleStatus.PagamentoAprovado;

            // Act
            var response = await client.PutAsync($"/api/sales/AtualizarVenda?id={saleId}&status={saleStatus}", null);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task UpdateSaleStatus_WithInvalidStatus_ReturnsBadRequestResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var saleId = "1";
            var invalidStatus = SaleStatus.Entregue;

            // Act
            var response = await client.PutAsync($"/api/sales/AtualizarVenda?id={saleId}&status={invalidStatus}", null);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
