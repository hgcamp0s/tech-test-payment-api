using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class SaleRequest
    {
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
    }
}