namespace tech_test_payment_api.Models
{
    public enum SaleStatus
    {
        AguardandoPagamento, 
        EnviadoTransportadora,
        PagamentoAprovado, 
        Entregue, 
        Cancelada
    }
}