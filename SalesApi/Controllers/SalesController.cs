using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SalesController : ControllerBase
    {
        private static List<Sale> sales = new List<Sale>();

        [HttpPost("RegistrarVenda")]
        public IActionResult RegisterSale(SaleRequest saleRequest)
        {
            if (saleRequest == null || saleRequest.Items == null || saleRequest.Items.Count == 0)
            {
                return BadRequest("Dados de venda inválidos");
            }

            var sale = new Sale
            {
                Id = Guid.NewGuid().ToString(),
                Seller = saleRequest.Seller,
                Date = DateTime.Now,
                Items = saleRequest.Items,
                Status = SaleStatus.AguardandoPagamento
            };

            sales.Add(sale);

            return Ok(sale);
        }

        [HttpGet("BuscarVenda")]
        public IActionResult GetSale(string id)
        {
            var sale = sales.Find(s => s.Items.Any(i => i.Id == id));
            // var sale = sales.Find(s => s.Id == id);

            if (sale == null)
            {
                return NotFound();
            }

            return Ok(sale);
        }

        [HttpPut("AtualizarVenda")]
        public IActionResult UpdateSaleStatus(string id, SaleStatus status)
        {
            var sale = sales.Find(s => s.Items.Any(i => i.Id == id));

            if (sale == null)
            {
                return NotFound();
            }

            if (!IsValidStatusTransition(sale.Status, status))
            {
                return BadRequest("Transição de status inválida");
            }

            sale.Status = status;

            return Ok(sale);
        }

        private bool IsValidStatusTransition(SaleStatus currentStatus, SaleStatus newStatus)
        {
            switch(currentStatus)
            {
                case SaleStatus.AguardandoPagamento:
                    return newStatus == SaleStatus.PagamentoAprovado || newStatus == SaleStatus.Cancelada;
                case SaleStatus.PagamentoAprovado:
                    return newStatus == SaleStatus.EnviadoTransportadora || newStatus == SaleStatus.Cancelada;
                case SaleStatus.EnviadoTransportadora:
                    return newStatus == SaleStatus.Entregue;
                default:
                    return false;
            }
        }
    }
}