using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace tech_test_payment_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Usado para configurar os serviços que serão usados pelo aplicativo.
        // Neste caso, estamos adicionando os serviços necessários para suportar WebAPI e o Swagger.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Sales API", Version = "v1" });
            });
        }

        // É usado para configurar o pipeline de solicitação HTTP do aplicativo
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) // Verifica se o aplicatitovp está sendo executado no ambiente de desenvolvimento
            {
                app.UseDeveloperExceptionPage(); // Se for, habilitamos a página de exceções do desenvolvedor
            }

            app.UseSwagger(); // Adiciona o middleware do Swagger para servir os arquivos JSON do Swagger gerados.
            app.UseSwaggerUI(c => // Adiciona o middleware do SwaggerUI para exibir a interface do Swagger no navegador
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sales API v1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting(); // Adiciona o middleware de roteamento ao pipeline de solicitação

            app.UseEndpoints(endpoints => // Configura os endpoints das rotas de solicitação
            {
                endpoints.MapControllers(); // Mapeia os controllers para os endpoints de solicitação
            });
        }
    }
}